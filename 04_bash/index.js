import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";
import { dirname } from "path";
import crypto from "crypto";

const readFileAndTrim = (filePath, callback) => {
  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      console.error(`Ошибка при чтении файла '${filePath}': ${err}`);
      process.exit(100);
    }
    const trimmedData = data.trim();
    callback(trimmedData);
  });
};

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

if (process.argv.length < 3) {
  console.error("Пожалуйста, укажите путь к файлу в аргументе команды.");
  process.exit(1);
}

const filePath = process.argv[2];
const absolutePath = path.isAbsolute(filePath) ? filePath : path.resolve(__dirname, filePath);

if (!fs.existsSync(absolutePath)) {
  console.error("Файл не существует.");
  process.exit(100);
}

const nameExtension = path.join();
const nameFile = path.basename(absolutePath, nameExtension);
const newSha256 = path.join(__dirname, `${nameFile}.sha256`);

readFileAndTrim(absolutePath, (data) => {
  readFileAndTrim(newSha256, (hashData) => {
    const sourceHash = crypto.createHash("sha256").update(data).digest("hex");
    if (sourceHash === hashData) {
      console.log("Хеш-сумма верна.");
    } else {
      console.error("Хеш-сумма не совпадает.");
      process.exit(101);
    }
  });
});
