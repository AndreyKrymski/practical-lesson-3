import fs from "fs";
import { fileURLToPath } from "url";
import { dirname, join, extname, basename } from "path";
import { createHash } from "crypto";
import fetch from "node-fetch";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

// Функция для чтения файла и удаления лишних символов
function readFileAndTrim(filePath, callback) {
  fs.readFile(filePath, "utf8", (err, data) => {
    if (err) {
      console.error(`Ошибка при чтении файла '${filePath}': ${err}`);
      process.exit(100); // Код ошибки 100
    }
    // Убираем лишние символы, например, перенос строки
    const trimmedData = data.trim();
    callback(trimmedData);
  });
}

// Функция для сравнения хешей
function compareHash(sourceHash, hashData) {
  if (sourceHash === hashData) {
    console.log("Хеш-сумма верна.");
  } else {
    console.error("Хеш-сумма не совпадает.");
    process.exit(101); // Код ошибки 101
  }
}
// Функция для загрузки файла из интернета
async function downloadFileFromUrl(url) {
  try {
    const response = await fetch(url);

    if (response.status === 200) {
      const data = await response.text();
      return data;
    } else {
      console.error(`Ошибка при загрузке файла: HTTP статус ${response.status}`);
      process.exit(101); // Код ошибки 101
    }
  } catch (error) {
    console.error(`Ошибка при загрузке файла: ${error.message}`);
    process.exit(101); // Код ошибки 101
  }
}

if (process.argv.length < 3) {
  console.error("Пожалуйста, укажите путь к файлу или URL файла в аргументе команды.");
  process.exit(1);
}

const sourcePath = process.argv[2];
const allPath = join(__dirname, sourcePath);

if (sourcePath.startsWith("http://") || sourcePath.startsWith("https://")) {
  // Это URL файла
  downloadFileFromUrl(sourcePath)
    .then((data) => {
      readFileAndTrim(join(__dirname, "internet.txt.sha256"), (dat) => {
        compareHash(createHash("sha256").update(data).digest("hex"), dat);
      });
    })
    .catch((error) => {
      console.error(`Ошибка при загрузке файла: ${error.message}`);
      process.exit(101); // Код ошибки 101
    });
} else {
  // Это локальный файл

  if (!fs.existsSync(allPath)) {
    console.error("Файл не существует.");
    process.exit(100); // Код ошибки 100
  }

  const ext = extname(sourcePath);
  const baseName = basename(sourcePath, ext);

  const hashPath = join(__dirname, `${baseName}${ext}.sha256`);
  console.log(hashPath);

  readFileAndTrim(sourcePath, (sourceData) => {
    readFileAndTrim(hashPath, (hashData) => {
      compareHash(createHash("sha256").update(sourceData).digest("hex"), hashData);
    });
  });
}
