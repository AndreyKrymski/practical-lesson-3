import http from "http";

const PORT = 8000;

const serverName = `http://localhost:${PORT}`;

const server = http.createServer((req, res) => {
  res.writeHead(200, { "Content-Type": "text/plain" });
  res.end("okay");
});

server.listen(PORT, () => {
  console.log(` Listening to ${serverName}`);
});
